import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/services.dart';
import 'package:flutter_line_sdk/flutter_line_sdk.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LineSignIN {
  Future<String> signInWithLine() async {
    await LineSDK.instance.setup("1655532404").then((_){});
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final result = await LineSDK.instance.login(scopes: ["profile"]);
      await Firestore.instance.collection("users").document(result.userProfile.userId).get().then((value) {
        if (value.data == null) {
          Firestore.instance.collection("users").document(result.userProfile.userId).setData({
            'name' : result.userProfile.displayName,
            'picture' : result.userProfile.pictureUrl,
            'role' : "Member"
          });
        } else {
          Firestore.instance.collection("users").document(result.userProfile.userId).updateData({
            'name' : result.userProfile.displayName,
            'picture' : result.userProfile.pictureUrl
          });
        }
      });
      DocumentSnapshot document = await Firestore.instance.collection('users').document(result.userProfile.userId).get();
      prefs.setString('uid', result.userProfile.userId);
      prefs.setString('loginWith', 'line');
      prefs.setString('name', result.userProfile.displayName);
      prefs.setString('picture', result.userProfile.pictureUrl);
      prefs.setString('role', document['role']);
      return 'LOGGED_IN';
    } catch (e) {
      return null;
    }
  }

   void signOutLine() async{
    try {
      await LineSDK.instance.logout();
    } on PlatformException catch (e) {
      print(e.message);
    }
  }
}