import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Google {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final GoogleSignIn googleSignIn = GoogleSignIn(
    scopes: [
      'email',
      'profile',
      'https://www.googleapis.com/auth/contacts.readonly',
    ],
  );

  Future<String> signInWithGoogle() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
      if (googleSignInAccount != null) {
        final GoogleSignInAuthentication googleSignInAuthentication =
            await googleSignInAccount.authentication;

        final AuthCredential credential = GoogleAuthProvider.getCredential(
          accessToken: googleSignInAuthentication.accessToken,
          idToken: googleSignInAuthentication.idToken,
        );

        final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
        await Firestore.instance.collection("users").document(user.uid).get().then((value) {
          if (value.data == null) {
            Firestore.instance.collection("users").document(user.uid).setData({
              'name' : user.displayName,
              'picture' : user.photoUrl,
              'role' : "Member"
            });
          } else {
            Firestore.instance.collection("users").document(user.uid).updateData({
              'name' : user.displayName,
              'picture' : user.photoUrl
            });
          }
        });
        DocumentSnapshot document = await Firestore.instance.collection('users').document(user.uid).get();
        await user.getIdToken().then((token){
          prefs.setString('uid', user.uid);
          prefs.setString('loginWith', 'google');
          prefs.setString('name', user.displayName);
          prefs.setString('picture', user.photoUrl);
          prefs.setString('role', document['role']);
        });
        return 'LOGGED_IN';
      }
      } catch (e) {
        return null;
      }
    return null;
  }

  void signOutGoogle() async{
    await googleSignIn.signOut();
    await _auth.signOut();
  }
}