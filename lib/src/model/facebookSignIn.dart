import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Facebook {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FacebookLogin facebookLogin = FacebookLogin();

  Future<String> signInWithFacebook() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      final FacebookLoginResult result = await facebookLogin.logIn(['email',"public_profile"]);
      String token = result.accessToken.token;
      final graphResponse = await http.get('https://graph.facebook.com/v2.12/me?fields=name,picture.height(200),first_name,last_name,email&access_token=$token');
      final profile = jsonDecode(graphResponse.body);
      final AuthCredential credential = FacebookAuthProvider.getCredential(
        accessToken: result.accessToken.token
      );
      final FirebaseUser user = (await _auth.signInWithCredential(credential)).user;
      await Firestore.instance.collection("users").document(user.uid).get().then((value) {
        if (value.data == null) {
          Firestore.instance.collection("users").document(user.uid).setData({
            'name' : user.displayName,
            'picture' : profile['picture']['data']['url'],
            'role' : "Member"
          });
        } else {
          Firestore.instance.collection("users").document(user.uid).updateData({
            'name' : user.displayName,
            'picture' : profile['picture']['data']['url']
          });
        }
      });
      DocumentSnapshot document = await Firestore.instance.collection('users').document(user.uid).get();
      await user.getIdToken().then((token){
        prefs.setString('uid', user.uid);
        prefs.setString('loginWith', 'facebook');
        prefs.setString('name', user.displayName);
        prefs.setString('picture', profile['picture']['data']['url']);
        prefs.setString('role', document['role']);
      });
      return 'LOGGED_IN';
    } catch (e) {
      print(e);
      return null;
    }
  }

  Future signOutFacebook() async {
    await facebookLogin.logOut();
    await _auth.signOut();
  }
}
