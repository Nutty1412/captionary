import 'package:captionary/src/app.dart';
import 'package:captionary/src/controller/signIn.dart';
import 'package:captionary/src/data/importData.dart';
import 'package:captionary/src/screen/addCaption.dart';
import 'package:captionary/src/screen/feedCaption.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'homeNotLogin.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> with AutomaticKeepAliveClientMixin{
  @override
  bool get wantKeepAlive => true;
  String name,picture,role,theme,color,loginWith;
  Color colorThemeApp;
  bool loadUserProfile = true;
  @override
  void initState() {
    loadProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    super.build(context);
    return Scaffold(
      body: Center(
        child: loadUserProfile ? Center(child: CircularProgressIndicator()) : Container(
          child: role == null ? Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image.asset('assets/images/profileNotLogin.png'),
              Container(
                child: RaisedButton(
                  color: colorThemeApp,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                  ),
                  onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeNotLogin())),
                  child: Text(
                    'เข้าสู่ระบบ',
                    style: TextStyle(
                      color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
                    ),
                  ),
                ),
              )
            ],
          ) : Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Image.network(
                  picture,
                  fit: BoxFit.fill,
                  width: loginWith == 'line' ? 200 : null,
                  loadingBuilder:(BuildContext context, Widget child,ImageChunkEvent loadingProgress) {
                  if (loadingProgress == null) return child;
                    return Center(
                      child: CircularProgressIndicator(
                      value: loadingProgress.expectedTotalBytes != null ?
                        loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes
                        : null,
                      ),
                    );
                  },
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 10),
                child: Text(
                  name,
                  style: TextStyle(
                    color: colorThemeApp,
                    fontWeight: FontWeight.bold
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: size.width * 0.2),
                child: Column(
                  children: [
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('purple');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFF8F00F2),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('blue');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFF00CFFB),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('green');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFF5CFF00),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('yellow');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFFFDFB00),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('orange');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFFFDAE32),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('red');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFFFF0C12),
                            ),
                          ),
                        ),
                        GestureDetector(
                          onTap: () async {
                            changeColorApp('pink');
                          },
                          child: Container(
                            width: size.width * 0.06,
                            height: size.width * 0.06,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: Color(0xFFFD3A69),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              SizedBox(height: 10),
              Container(
                width: size.width * 0.6,
                child: RaisedButton(
                  color: colorThemeApp,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      theme == 'light' ? 'Dark Mode' : 'Light Mode',
                      style: TextStyle(
                        color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black
                      ),
                    ),
                  ),
                  onPressed: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    if (theme == 'light') {
                      prefs.setString('theme', 'dark');
                    } else {
                      prefs.setString('theme', 'light');
                    }
                    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => App(
                      defaultRoute: 4,
                    )), (Route<dynamic> route) => false);
                  }
                ),
              ),
              Container(
                width: size.width * 0.6,
                child: RaisedButton(
                  color: colorThemeApp,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      'เพิ่มแคปชั่นเข้าสู่ระบบ',
                      style: TextStyle(
                        color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black
                      ),
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(context, MaterialPageRoute(builder: (context) => AddCaption()));
                  }
                ),
              ),
              Container(
                width: size.width * 0.6,
                child: RaisedButton(
                  color: colorThemeApp,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: 12),
                    child: Text(
                      'ออกจากระบบ',
                      style: TextStyle(
                        color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black
                      ),
                    ),
                  ),
                  onPressed: () async {
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.remove("uid");
                    prefs.remove("role");
                    prefs.remove("name");
                    prefs.remove("picture");
                    if (prefs.getString('loginWith') == 'google') {
                      prefs.remove('loginWith');
                      Auth().signOutGoogle().then((value) => Phoenix.rebirth(context));
                    } else if (prefs.getString('loginWith') == 'facebook') {
                      prefs.remove('loginWith');
                      Auth().signOutFacebook().then((value) => Phoenix.rebirth(context));
                    } else {
                      prefs.remove('loginWith');
                      Auth().signOutLine().then((value) => Phoenix.rebirth(context));
                    }
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  changeColorApp(color) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('color', color);
    Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(builder: (context) => App(
      defaultRoute: 4,
    )), (Route<dynamic> route) => false);
  }

  loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    picture = prefs.getString('picture');
    name = prefs.getString('name');
    role = prefs.getString('role');
    theme = prefs.getString('theme');
    loginWith = prefs.getString('loginWith');
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}
