import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'addCaptionToImage.dart';

class CommentPage extends StatefulWidget {
  CommentPage({
    @required this.captionID,
    @required this.uid
  });

  final captionID,uid;
  @override
  _CommentPageState createState() => _CommentPageState();
}

class _CommentPageState extends State<CommentPage> {
  int countComment = 0;
  bool loading = true;
  String uid,captionID,color;
  Color colorThemeApp;
  TextEditingController comment = TextEditingController();
  @override
  void initState() {
    captionID = this.widget.captionID;
    uid = this.widget.uid;
    loadProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double width  = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        title: Text('Comment'),
      ),
      body: loading ? Center(child: CircularProgressIndicator()) : GestureDetector(
        onTap: (){
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: StreamBuilder<DocumentSnapshot>(
            stream: Firestore.instance.collection('captions').document(captionID).snapshots(),
            builder: (context, AsyncSnapshot<DocumentSnapshot> snapshot){
              if (snapshot.hasData) {
                var data = snapshot.data.data;
                bool like = false;
                if (data['like'].length != 0) {
                  for (int i = 0; i < data['like'].length ; i++) {
                    if (data['like'][i] == uid) {
                      like = true;
                    }
                  }
                }
                return Column(
                  children: [
                    Card(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          Padding(
                            padding: EdgeInsets.only(left: 10,top: 5),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                data['postBy'] == 'Admin' ? Row(
                                  children: [
                                    CircleAvatar(
                                      backgroundColor: Colors.transparent,
                                      backgroundImage: AssetImage('assets/icons/admin.png'),
                                    ),
                                    Text(' ' + data['postBy']),
                                  ],
                                ) : StreamBuilder(
                                  stream: Firestore.instance.collection('users').document(data['postBy']).snapshots(),
                                  builder: (context,snapshot) {
                                    if (snapshot.hasData) {
                                      return Row(
                                        children: [
                                          CircleAvatar(
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: NetworkImage(snapshot.data['picture']),
                                          ),
                                          Text(' ' + snapshot.data['name']),
                                        ],
                                      );
                                    }
                                    return Container();
                                  },
                                ),
                                Padding(
                                  padding: EdgeInsets.only(right: 10),
                                  child: Text(
                                    '#' + data['category'],
                                    style: TextStyle(
                                      color: colorThemeApp
                                    )
                                  ),
                                )
                              ],
                            ),
                          ),
                          Row(
                            children: [
                              Container(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: Padding(
                                  padding: EdgeInsets.only(left: 15),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        data['message'],
                                        style: TextStyle(fontSize: 20),
                                      ),
                                      Text(
                                        'Cr :: ' + data['credit'],
                                        style: TextStyle(color: Colors.black54),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                              Expanded(
                                child: GestureDetector(
                                  onTap: () {
                                    if (data['status'] == 'canReport') {
                                      Firestore.instance.collection("captions").document(snapshot.data.documentID).updateData({
                                        'status' : 'review'
                                      });
                                    }
                                  },
                                  child: Icon(
                                    data['status'] == 'canReport' ? Icons.error_rounded : data['status'] == 'review' ? Icons.preview_outlined : Icons.check_circle,
                                    color: data['status'] == 'canReport' ? Colors.grey: data['status'] == 'review' ? Colors.orange : Colors.green,
                                  ),
                                )
                              )
                            ],
                          ),
                          SizedBox(height: 5),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 15),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Row(
                                  children: [
                                    Icon(
                                      Icons.favorite,
                                      color: Colors.red,
                                      size: 16,
                                    ),
                                    Text(' '+data['like'].length.toString())
                                  ],
                                ),
                                Row(
                                  children: [
                                    Text('แชร์ '+data['countShare'].toString() + ' ครั้ง')
                                  ],
                                )
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceAround,
                              children: [
                                GestureDetector(
                                  onTap: () async {
                                    var listUserLike = data['like'];
                                    if (like) {
                                      listUserLike.remove(uid);
                                    } else {
                                      listUserLike.add(uid);
                                    }
                                    await Firestore.instance.collection("captions").document(snapshot.data.documentID).updateData({
                                      'like' : listUserLike
                                    });
                                  },
                                  child: Icon(
                                    like ? Icons.favorite :Icons.favorite_border,
                                    color: like ? Colors.red : Colors.grey
                                  ),
                                ),
                                GestureDetector(
                                  onTap: () {
                                    Clipboard.setData(new ClipboardData(text: data['message']));
                                    Scaffold.of(context)
                                      ..removeCurrentSnackBar()
                                      ..showSnackBar(SnackBar(content: Text("คัดลอกข้อความแล้ว")));
                                  },
                                  child: Icon(
                                    Icons.copy,color: Colors.grey
                                  )
                                ),
                                GestureDetector(
                                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddCaptionToImage(
                                    caption: data['message'],
                                    uid: snapshot.data.documentID,
                                  ))),
                                  child: Icon(Icons.edit_outlined,color: Colors.grey),
                                )
                              ],
                            ),
                          ),
                          Divider(),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8,0,8,8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                Container(
                                  width: width * 0.8,
                                  child: TextFormField(
                                    controller: comment,
                                    onChanged: (value) {
                                      setState(() {
                                        countComment = value.length;
                                      });
                                    },
                                    decoration: InputDecoration(
                                      hintText: "แสดงความคิดเห็น . . .",
                                      fillColor: Colors.white,
                                      border: InputBorder.none
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: countComment == 0 ? null : () async {
                                    var allComment = data['comment'];
                                    allComment.add(
                                      {
                                        'commentBy' : uid,
                                        'comment' : comment.text
                                      }
                                    );
                                    await Firestore.instance.collection("captions").document(snapshot.data.documentID).updateData({
                                      'comment' : allComment,
                                    });
                                    comment.clear();
                                    setState(() {
                                      countComment = 0;
                                    });
                                  },
                                  child: Icon(
                                    Icons.send,
                                    color: countComment == 0 ? Colors.grey : Colors.black
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: ListView.builder(
                        itemCount: data['comment'].length,
                        itemBuilder: (context, j) {
                          int i = data['comment'].length-1-j;
                          return StreamBuilder(
                            stream: Firestore.instance.collection('users').document(data['comment'][i]['commentBy']).snapshots(),
                            builder: (context ,snapshot) {
                              if (snapshot.hasData) {
                                return ListTile(
                                  leading: CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    backgroundImage: NetworkImage(snapshot.data['picture']),
                                  ),
                                  title: Text(snapshot.data['name']),
                                  subtitle: Text(data['comment'][i]['comment']),
                                );
                              }
                              return LinearProgressIndicator();
                            }
                          );
                        },
                      ),
                    )
                  ],
                );
              }
              return CircularProgressIndicator();
            }
          ),
        ),
      )
    );
  }

  loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loading = false;
    });
  }
}