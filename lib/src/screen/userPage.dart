import 'package:flutter/material.dart';
import 'package:salomon_bottom_bar/salomon_bottom_bar.dart';
import 'feedCaption.dart';
import 'hashtag.dart';
import 'imageGallery.dart';
import 'profile.dart';
import 'ranking.dart';

class UserPage extends StatefulWidget {
  UserPage({
    this.defaultRoute,
    this.color
  });

  final defaultRoute,color;
  @override
  _UserPageState createState() => _UserPageState();
}

class _UserPageState extends State<UserPage> {
  int _selectedIndex;
  Color color;
  final List<Widget> _pages = [
    FeedCaption(),
    Hashtag(),
    ImageGallery(),
    Ranking(),
    Profile()
  ];

  PageController pageController;

  @override
  void initState() {
    super.initState();
    _selectedIndex = this.widget.defaultRoute;
    color = this.widget.color;
    pageController = PageController(initialPage: _selectedIndex);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  _onTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
    pageController.jumpToPage(index);
  }

  void onPageChanged(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text("Captionary"),
      ),
      body: PageView(
        children: _pages,
        controller: pageController,
        onPageChanged: onPageChanged,
      ),
      bottomNavigationBar: SafeArea(
        child: SalomonBottomBar(
          currentIndex: _selectedIndex,
          onTap: _onTapped,
          items: [
            SalomonBottomBarItem(
              icon: Icon(Icons.home),
              title: Text(
                "หน้าหลัก",
                style: TextStyle(
                  fontFamily: 'Mali',
                  fontSize: size.height * 0.015
                ),
              ),
              selectedColor: color,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.tag),
              title: Text(
                "หมวดหมู่",
                style: TextStyle(
                  fontFamily: 'Mali',
                  fontSize: size.height * 0.015
                ),
              ),
              selectedColor: color,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.image),
              title: Text(
                "แกลอรี่",
                style: TextStyle(
                  fontFamily: 'Mali',
                  fontSize: size.height * 0.015
                ),
              ),
              selectedColor: color,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.elevator),
              title: Text(
                "จัดอันดับ",
                style: TextStyle(
                  fontFamily: 'Mali',
                  fontSize: size.height * 0.015
                ),
              ),
              selectedColor: color,
            ),
            SalomonBottomBarItem(
              icon: Icon(Icons.person),
              title: Text(
                "โปรไฟล์",
                style: TextStyle(
                  fontFamily: 'Mali',
                  fontSize: size.height * 0.015
                ),
              ),
              selectedColor: color,
            ),
          ],
        ),
      ),
    );
  }
}