import 'package:captionary/src/controller/signIn.dart';
import 'package:captionary/src/widget/buttonForSocialLogin.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:shared_preferences/shared_preferences.dart';

class HomeNotLogin extends StatefulWidget {
  @override
  _HomeNotLoginState createState() => _HomeNotLoginState();
}

class _HomeNotLoginState extends State<HomeNotLogin> {
  bool loadUserProfile = true;
  String color;
  Color colorThemeApp;
  @override
  void initState() {
    loadTheme();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black
        ),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Image.asset('assets/images/mobileLogin.png'),
          InkWell(
            onTap: () {
              Auth().signInWithFacebook().then((result) {
                print(result);
                if (result != null) {
                  Phoenix.rebirth(context);
                } else {
                  print('Cancel');
                }
              });
            },
            child: ButtonForSocialLogin(
              color: colorThemeApp,
              icon: 'assets/icons/facebook.png',
              text: 'เข้าสู่ระบบด้วย Facebook',
              fontColor: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(vertical: width * 0.05),
            child: InkWell(
              onTap: () {
                Auth().signInWithGoogle().then((result) {
                  if (result != null) {
                    Phoenix.rebirth(context);
                  } else {
                    print('Cancel');
                  }
                });
              },
              child: ButtonForSocialLogin(
                color: colorThemeApp,
                icon: 'assets/icons/google.png',
                text: 'เข้าสู่ระบบด้วย Google',
                fontColor: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
              ),
            ),
          ),
          InkWell(
            onTap: () {
              Auth().signInWithLine().then((result){
                if (result != null) {
                  Phoenix.rebirth(context);
                } else {
                  print('Cancel');
                }
              });
            },
            child: ButtonForSocialLogin(
              color: colorThemeApp,
              icon: 'assets/icons/line.png',
              text: 'เข้าสู่ระบบด้วย Line',
              fontColor: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
            ),
          ),
        ],
      ),
    );
  }

  loadTheme() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}