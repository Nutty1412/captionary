import 'package:captionary/src/screen/homeNotLogin.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AddCaption extends StatefulWidget {
  @override
  _AddCaptionState createState() => _AddCaptionState();
}

class _AddCaptionState extends State<AddCaption> {
  String message,credit;
  String uid,role,color;
  Color colorThemeApp;
  bool loadUserProfile = true;
  String dropdownValue = 'haha';
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    loadProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('เพิ่มแคปชั่นเข้าสู่ระบบ'),
      ),
      body: loadUserProfile ? Center(child: CircularProgressIndicator()) : Container(
      child: uid == null ? Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image.asset('assets/images/notLogin.png'),
          Container(
            child: RaisedButton(
              color: colorThemeApp,
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)
              ),
              onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeNotLogin())),
              child: Text(
                'เข้าสู่ระบบ',
                style: TextStyle(
                  color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
                ),
              ),
            ),
          )
        ],
      ) : SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: MediaQuery.of(context).size.width * 0.05),
            child: Column(
              children: [
                Image.asset('assets/images/addCaption.png'),
                TextFormField(
                  keyboardType: TextInputType.multiline,
                  minLines: 1,
                  maxLines: 5,
                  cursorColor: colorThemeApp,
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    prefixIcon: Icon(Icons.message),
                    labelText: 'ข้อความ'
                  ),
                  maxLength: 100,
                  onSaved: (value) => message = value,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'กรุณากรอกข้อความที่ต้องการแชร์';
                    }
                    return null;
                  },
                ),
                TextFormField(
                  cursorColor: colorThemeApp,
                  decoration: InputDecoration(
                    prefixIcon: Icon(Icons.find_in_page),
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    labelText: 'แหล่งที่มา'
                  ),
                  onSaved: (value) => credit = value,
                  validator: (value) {
                    if (value.isEmpty) {
                      return 'กรุณากรอกแหล่งที่มา';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 20),
                DropdownButtonFormField(
                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
                    errorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
                    prefixIcon: Icon(Icons.category),
                    labelText: 'หมวดหมู่'
                  ),
                  items: <String>['haha', 'loveboy', 'Sadboy'].map((String category) {
                    return new DropdownMenuItem(
                      value: category,
                      child: Text(category)
                    );
                  }).toList(),
                  onChanged: (newValue) {
                    setState(() => dropdownValue = newValue);
                  },
                  value: dropdownValue
                ),
                SizedBox(height: 20),
                RaisedButton(
                  color: colorThemeApp,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(20)
                  ),
                  child: Text(
                    'ส่งเข้าระบบ',
                    style: TextStyle(
                      color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black
                    ),
                  ),
                  onPressed: () async {
                    if (_formKey.currentState.validate()) {
                      _formKey.currentState.save();
                      await Firestore.instance.collection("captions").document().setData({
                        "message": message,
                        "category": dropdownValue,
                        "credit": credit,
                        "postBy": role == 'Admin' ? 'Admin' : uid,
                        "status": "canReport",
                        "like": [],
                        "comment": [],
                        "countShare" : 0,
                        "dateCreate" : DateTime.now().microsecondsSinceEpoch,
                        "lastUpdate": DateTime.now().microsecondsSinceEpoch
                      });
                      _formKey.currentState.reset();
                      Scaffold.of(context)
                        ..removeCurrentSnackBar()
                        ..showSnackBar(SnackBar(content: Text("เพิ่มแคปชั่นเข้าสู่ระบบแล้ว")));
                    }
                  }
                )
              ],
            ),
          ),
        ),
      ),
    ));
  }

  loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString('uid');
    role = prefs.getString('role');
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}