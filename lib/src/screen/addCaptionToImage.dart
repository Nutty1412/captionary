import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'dart:async';
import 'dart:ui' as ui;

import 'package:shared_preferences/shared_preferences.dart';

class AddCaptionToImage extends StatefulWidget {
  AddCaptionToImage({
    @required this.caption,
    @required this.uid
  });

  final caption,uid;
  @override
  _AddCaptionToImageState createState() => _AddCaptionToImageState();
}

class _AddCaptionToImageState extends State<AddCaptionToImage> {
  Color colorThemeApp;
  Offset offset = Offset.zero;
  double _fontSize = 16;
  double _paddingSize = 50;
  var _image;
  Color pickerColor = Color(0xffffffff);
  Color currentColor = Color(0xffffffff);
  bool bold = false,italic = false,left = false,center = true,right = false,shadow = false,border = false;
  bool selectCamera = false,selectiImage = false;
  String font = 'Mali';
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool loading = false;
  bool saveGallery = false;
  bool sharePublic = false;
  bool sharePrivate = false;
  String color;
  bool loadUserProfile = true;

  void changeColor(Color color) {
    setState(() {
      pickerColor = color;
      saveGallery = false;
      sharePublic = false;
      sharePrivate = false;
    });
  }

  @override
  void initState() {
    loadProfile();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('เพิ่มแคปชั่นเข้ารูปภาพ'),
      ),
      body:loadUserProfile ? Center(child: CircularProgressIndicator()) : Column(
        children: [
          Container(
            color: Colors.white,
            height: null,
            width: size.width,
            child: Center(
              child: _image == null ? Image.asset('assets/images/addImage.png') : RepaintBoundary(
                key: _globalKey,
                child: Stack(
                  children: [
                    Image.file(File(_image.path)),
                    Container(
                      child: Positioned(
                        left: offset.dx,
                        top: offset.dy,
                        child: GestureDetector(
                          onPanUpdate: (details) {
                            setState(() {
                              offset = Offset(offset.dx + details.delta.dx, offset.dy + details.delta.dy);
                              saveGallery = false;
                              sharePublic = false;
                              sharePrivate = false;
                            });
                          },
                          child: SizedBox(
                            width: size.width * (_paddingSize / 100),
                            child: Center(
                              child: Text(
                                this.widget.caption,
                                textAlign: center ? TextAlign.center : left ? TextAlign.left : TextAlign.right,
                                style: TextStyle(
                                  fontWeight: bold ? FontWeight.bold : null,
                                  fontStyle: italic ? FontStyle.italic : null,
                                  fontSize: _fontSize,
                                  color: pickerColor,
                                  backgroundColor: border ? Colors.black : null,
                                  fontFamily: font,
                                  shadows: shadow ? <Shadow>[
                                    Shadow(
                                      offset: Offset(1.5, 1.5),
                                      blurRadius: 5.0,
                                      color: Color.fromARGB(255, 0, 0, 0),
                                    ),
                                  ] : null
                                )
                              ),
                            ),
                          )
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ),
          ),
          Column(
            children: [
              Divider(),
              Column(
                children: [
                  Text('นำเข้ารูปภาพ',style: TextStyle(fontWeight: FontWeight.bold)),
                  SizedBox(height: 5),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: () async => getImage(size,true),
                        child: Row(
                          children: [
                            Icon(Icons.camera,color: selectCamera ? colorThemeApp : Colors.black),
                            Text(' กล้องถ่ายรูป',style: TextStyle(color: selectCamera ? colorThemeApp : Colors.black)),
                          ],
                        )
                      ),
                      GestureDetector(
                        onTap: () async => getImage(size,false),
                        child: Row(
                          children: [
                            Icon(Icons.image,color: selectiImage ? colorThemeApp : Colors.black),
                            Text(' รูปภาพ',style: TextStyle(color: selectiImage ? colorThemeApp : Colors.black)),
                          ],
                        )
                      ),
                    ],
                  ),
                  Divider(),
                ],
              ),
            ],
          ),
          if (_image != null)
            Expanded(
              child: ListView(
                children: [
                  Column(
                    children: [
                      Text('เปลี่ยนสีตัวอักษร',style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 3),
                      ColorPicker(
                        pickerColor: pickerColor,
                        onColorChanged: changeColor,
                        pickerAreaBorderRadius: BorderRadius.circular(10),
                        pickerAreaHeightPercent: 0.3,
                        showLabel: false,
                      ),
                    ],
                  ),
                  Divider(),
                  Column(
                    children: [
                      Text('เลือกแบบอักษร',style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 3),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Mali';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Mali',
                                color: font == 'Mali' ? colorThemeApp : Colors.black
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Mitr';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Mitr',
                                color: font == 'Mitr' ? colorThemeApp : Colors.black
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Athiti';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Athiti',
                                color: font == 'Athiti' ? colorThemeApp : Colors.black
                              ),
                            ),
                          )
                        ],
                      ),
                      SizedBox(height: 10),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Prompt';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Prompt',
                                color: font == 'Prompt' ? colorThemeApp : Colors.black
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Krub';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Krub',
                                color: font == 'Krub' ? colorThemeApp : Colors.black
                              ),
                            ),
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                font = 'Sriracha';
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Text(
                              'ตัวอย่าง',
                              style: TextStyle(
                                fontFamily: 'Sriracha',
                                color: font == 'Sriracha' ? colorThemeApp : Colors.black
                              ),
                            ),
                          )
                        ],
                      )
                    ],
                  ),
                  Divider(),
                  Column(
                    children: [
                      Text('รูปแบบตัวอักษร',style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 8),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                bold = !bold;
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Icon(
                              Icons.format_bold,
                              color: bold ? colorThemeApp : Colors.black,
                            )
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                italic = !italic;
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Icon(
                              Icons.format_italic,
                              color: italic ? colorThemeApp : Colors.black,
                            )
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                center = false;
                                left = true;
                                right = false;
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Icon(
                              Icons.format_align_left,
                              color: left ? colorThemeApp : Colors.black
                            )
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                center = true;
                                left = false;
                                right = false;
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Icon(
                              Icons.format_align_center,
                              color: center ? colorThemeApp : Colors.black,
                            )
                          ),
                          GestureDetector(
                            onTap: () {
                              setState(() {
                                center = false;
                                left = false;
                                right = true;
                                saveGallery = false;
                                sharePublic = false;
                                sharePrivate = false;
                              });
                            },
                            child: Icon(
                              Icons.format_align_right,
                              color: right ? colorThemeApp : Colors.black,
                            )
                          ),
                        ],
                      ),
                    ],
                  ),
                  Divider(),
                  Column(
                    children: [
                      Text('ขนาดตัวอักษร',style: TextStyle(fontWeight: FontWeight.bold)),
                      Slider(
                        activeColor: colorThemeApp,
                        inactiveColor: Colors.black12,
                        value: _fontSize,
                        min: 8,
                        max: 40,
                        divisions: 32,
                        label: _fontSize.round().toString(),
                        onChanged: (double value) {
                          setState(() {
                            _fontSize = value;
                            saveGallery = false;
                            sharePublic = false;
                            sharePrivate = false;
                          });
                        },
                      )
                    ],
                  ),
                  Divider(),
                  Column(
                    children: [
                      Text('ระยะการขึ้นบรรทัดใหม่',style: TextStyle(fontWeight: FontWeight.bold)),
                      Slider(
                        activeColor: colorThemeApp,
                        inactiveColor: Colors.black12,
                        value: _paddingSize,
                        min: 20,
                        max: 100,
                        divisions: 80,
                        label: _paddingSize.round().toString(),
                        onChanged: (double value) {
                          setState(() {
                            _paddingSize = value;
                            saveGallery = false;
                            sharePublic = false;
                            sharePrivate = false;
                          });
                        },
                      )
                    ],
                  ),
                  Divider(),
                  Column(
                    children: [
                      Text('รูปแบบพื้นหลัง',style: TextStyle(fontWeight: FontWeight.bold)),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                value: shadow,
                                onChanged: (value) {
                                  setState(() {
                                    shadow = value;
                                    saveGallery = false;
                                    sharePublic = false;
                                    sharePrivate = false;
                                  });
                                },
                              ),
                              Text('เงาตัวอักษร')
                            ],
                          ),
                          Row(
                            children: [
                              Checkbox(
                                value: border,
                                onChanged: (value) {
                                  setState(() {
                                    border = value;
                                    saveGallery = false;
                                    sharePublic = false;
                                    sharePrivate = false;
                                  });
                                },
                              ),
                              Text('พื้นหลังตัวอักษร')
                            ],
                          )
                        ],
                      )
                    ],
                  ),
                  loading ? Center(child: CircularProgressIndicator()) : Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: size.width * 0.1),
                        child: RaisedButton(
                          color: colorThemeApp,
                          padding: EdgeInsets.symmetric(vertical: size.width * 0.04),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)
                          ),
                          onPressed: saveGallery ? null : () async => _capturePng() ,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.file_download,color: Colors.white),
                              Text(
                                ' บันทึกรูปภาพลงแกลอรี่',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.0125),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: size.width * 0.1),
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: size.width * 0.04),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)
                          ),
                          color: colorThemeApp,
                          onPressed: sharePublic ? null : () async => shareImageToPublic('public'),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.share,color: Colors.white),
                              Text(
                                ' แชร์เข้าสู่ระบบ',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(height: size.height * 0.0125),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: size.width * 0.1),
                        child: RaisedButton(
                          padding: EdgeInsets.symmetric(vertical: size.width * 0.04),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50)
                          ),
                          color: colorThemeApp,
                          onPressed: sharePrivate ? null : () async => shareImageToPublic('private'),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Icon(Icons.share,color: Colors.white),
                              Text(
                                ' แชร์เข้าแกลอรี่ส่วนตัว',
                                style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.bold
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            )
        ],
      )
    );
  }

  shareImageToPublic(status) async {
    setState(() {
      loading = true;
    });
    SharedPreferences prefs = await SharedPreferences.getInstance();
    RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
    ui.Image image = await boundary.toImage(pixelRatio: 1.5);
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    var pngBytes = byteData.buffer.asUint8List();
    String img64 = base64Encode(pngBytes);
    DocumentSnapshot document = await Firestore.instance.collection('captions').document(this.widget.uid).get();
    int countShare = document['countShare'];
    Firestore.instance.collection("captions").document(this.widget.uid).updateData({
      'countShare' : countShare+=1
    });
    await Firestore.instance.collection("shareImage").add({
      'image' : img64,
      'uploader' : prefs.get('uid'),
      'status' : status,
      'form' : this.widget.caption,
      'vertify' : 'canReport'
    });
    if (status == 'private') {
      setState(() {
        loading = false;
        sharePrivate = true;
      });
    } else {
      setState(() {
        loading = false;
        sharePublic = true;
      });
    }
  }

  GlobalKey _globalKey = new GlobalKey();

  Future _capturePng() async {
    setState(() {
      loading = true;
    });
    try {
      RenderRepaintBoundary boundary = _globalKey.currentContext.findRenderObject();
      ui.Image image = await boundary.toImage(pixelRatio: 3.0);
      ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
      var pngBytes = byteData.buffer.asUint8List();
      await ImageGallerySaver.saveImage(
        Uint8List.fromList(pngBytes),
        quality: 80,
        name: DateTime.now().millisecondsSinceEpoch.toString()
      );
      DocumentSnapshot document = await Firestore.instance.collection('captions').document(this.widget.uid).get();
      int countShare = document['countShare'];
      Firestore.instance.collection("captions").document(this.widget.uid).updateData({
        'countShare' : countShare+=1
      });
    } catch (e) {
      print(e);
    }
    setState(() {
      loading = false;
      saveGallery = true;
    });
  }

  Future<void> getImage(size,camera) async {
    final ImagePicker _picker = ImagePicker();
    var image = camera ? await _picker.getImage(
      source: ImageSource.camera,
      maxHeight: size.height * 0.5
    ) : await _picker.getImage(
      source: ImageSource.gallery,
      maxHeight: size.height * 0.5
    );
    if (image != null) {
      setState(() {
        offset = Offset.zero;
        _image = image;
          saveGallery = false;
          sharePublic = false;
          sharePrivate = false;
        if (camera) {
          selectCamera = true;
          selectiImage = false;
        } else {
          selectCamera = false;
          selectiImage = true;
        }
      });
    }
  }

  loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}