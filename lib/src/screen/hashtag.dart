import 'package:captionary/src/screen/addCaptionToImage.dart';
import 'package:captionary/src/screen/commentPage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'homeNotLogin.dart';

class Hashtag extends StatefulWidget {
  @override
  _HashtagState createState() => _HashtagState();
}

class _HashtagState extends State<Hashtag> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  String uid,searching = "",category = "ทั้งหมด",role,color;
  Color colorThemeApp;
  bool loadUserProfile = true;
  var _controller = ScrollController();
  int limit = 10;
  @override
  void initState() {
    loadProfile();
    super.initState();
    _controller.addListener(() {
      if (_controller.position.atEdge) {
        if (_controller.position.pixels == 0) {
          print('top');
        } else {
          setState(() {
            limit = limit + 5;
          });
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return loadUserProfile ? Center(child: CircularProgressIndicator()) : Column(
      children: [
        Padding(
          padding: EdgeInsets.all(10),
          child: DropdownButtonFormField(
            decoration: InputDecoration(
              prefixIcon: Icon(Icons.tag,color: colorThemeApp,),
              focusedBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey)),
              errorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
              focusedErrorBorder: OutlineInputBorder(borderSide: BorderSide(color: colorThemeApp)),
              hintText: 'หมวดหมู่',
            ),
            items: <String>['ทั้งหมด','haha', 'loveboy', 'Sadboy'].map((String category) {
              return new DropdownMenuItem(
                value: category,
                child: Text(category)
              );
            }).toList(),
            onChanged: (newValue) {
              setState(() => category = newValue);
            },
            value: category
          ),
        ),
        Expanded(
          child: StreamBuilder(
            stream: category == 'ทั้งหมด' ?
              Firestore.instance.collection('captions').orderBy('dateCreate', descending: true).limit(limit).snapshots() :
              Firestore.instance.collection('captions').where("category",isEqualTo: category).orderBy('dateCreate', descending: true).limit(limit).snapshots(),
            builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot){
              if (snapshot.hasData) {
                return ListView(
                  controller: _controller,
                  children: snapshot.data.documents.map((document) {
                    var data = document.data;
                    bool like = false;
                    if (data['like'].length != 0) {
                      for (int i = 0; i < data['like'].length ; i++) {
                        if (data['like'][i] == uid) {
                          like = true;
                        }
                      }
                    }
                    return Padding(
                      padding: EdgeInsets.symmetric(horizontal: 5),
                      child: GestureDetector(
                        onTap: () => role == null ? null : Navigator.push(context, MaterialPageRoute(builder: (context) => CommentPage(
                          uid: uid,
                          captionID: document.documentID,
                        ))),
                        onLongPress: () {
                          Clipboard.setData(new ClipboardData(text: data['message']));
                          Scaffold.of(context)
                            ..removeCurrentSnackBar()
                            ..showSnackBar(SnackBar(content: Text("คัดลอกข้อความแล้ว")));
                        },
                        child: Card(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(
                                padding: EdgeInsets.only(left: 10,top: 5),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    data['postBy'] == 'Admin' ? Row(
                                      children: [
                                        CircleAvatar(
                                          backgroundColor: Colors.transparent,
                                          backgroundImage: AssetImage('assets/icons/admin.png'),
                                        ),
                                        Text(
                                          ' ' + data['postBy'],
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold
                                          ),
                                        ),
                                      ],
                                    ) : StreamBuilder(
                                      stream: Firestore.instance.collection('users').document(data['postBy']).snapshots(),
                                      builder: (context,snapshot) {
                                        if (snapshot.hasData) {
                                          return Row(
                                            children: [
                                              CircleAvatar(
                                                backgroundColor: Colors.transparent,
                                                backgroundImage: NetworkImage(snapshot.data['picture']),
                                              ),
                                              Text(
                                                ' ' + snapshot.data['name'],
                                                style: TextStyle(
                                                  fontWeight: FontWeight.bold
                                                ),
                                              ),
                                            ],
                                          );
                                        }
                                        return Container();
                                      },
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(right: 10),
                                      child: Text(
                                        '#' + data['category'],
                                        style: TextStyle(
                                          color: colorThemeApp,
                                          fontWeight: FontWeight.bold
                                        )
                                      ),
                                    )
                                  ],
                                ),
                              ),
                              Row(
                                children: [
                                  Container(
                                    width: MediaQuery.of(context).size.width * 0.8,
                                    child: Padding(
                                      padding: EdgeInsets.only(left: 15),
                                      child: Column(
                                        crossAxisAlignment: CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            data['message'],
                                            style: TextStyle(fontSize: 20),
                                          ),
                                          Text(
                                            'Cr :: ' + data['credit'],
                                            style: TextStyle(color: Colors.black54),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                  Expanded(
                                    child: GestureDetector(
                                      onTap: () {
                                        if (data['status'] == 'canReport') {
                                          Firestore.instance.collection("captions").document(document.documentID).updateData({
                                            'status' : 'review'
                                          });
                                        }
                                      },
                                      child: Icon(
                                        data['status'] == 'canReport' ? Icons.error_rounded : data['status'] == 'review' ? Icons.preview_outlined : Icons.check_circle,
                                        color: data['status'] == 'canReport' ? Colors.grey: data['status'] == 'review' ? Colors.orange : Colors.green,
                                      ),
                                    )
                                  )
                                ],
                              ),
                              SizedBox(height: 5),
                              Padding(
                                padding: EdgeInsets.symmetric(horizontal: 15),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Row(
                                      children: [
                                        Icon(
                                          Icons.favorite,
                                          color: Colors.red,
                                          size: 16,
                                        ),
                                        Text(' '+data['like'].length.toString())
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Row(
                                          children: [
                                            Text('ความคิดเห็น ' + data['comment'].length.toString() + ' รายการ')
                                          ],
                                        ),
                                        Text('  '),
                                        Row(
                                          children: [
                                            Text('แชร์ '+data['countShare'].toString() + ' ครั้ง')
                                          ],
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                              Divider(),
                              Padding(
                                padding: EdgeInsets.only(bottom: 10),
                                child: role == null ? Center(child: GestureDetector(
                                  onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeNotLogin())),
                                  child: Text(
                                    'กรุณาเข้าสู่ระบบ !!',
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold
                                    ),
                                  ),
                                )) : Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        var listUserLike = data['like'];
                                        if (like) {
                                          listUserLike.remove(uid);
                                        } else {
                                          listUserLike.add(uid);
                                        }
                                        await Firestore.instance.collection("captions").document(document.documentID).updateData({
                                          'like' : listUserLike
                                        });
                                      },
                                      child: Icon(
                                        like ? Icons.favorite :Icons.favorite_border,
                                        color: like ? Colors.red : Colors.grey
                                      ),
                                    ),
                                    GestureDetector(
                                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => CommentPage(
                                        uid: uid,
                                        captionID: document.documentID,
                                      ))),
                                      child: Icon(Icons.comment_outlined,color: Colors.grey)
                                    ),
                                    GestureDetector(
                                      onTap: () => Navigator.push(context, MaterialPageRoute(builder: (context) => AddCaptionToImage(
                                        caption: data['message'],
                                        uid: document.documentID,
                                      ))),
                                      child: Icon(Icons.edit_outlined,color: Colors.grey),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    );
                }).toList());
              }
              return Center(child: CircularProgressIndicator());
            }
          ),
        ),
      ],
    );
  }

  loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString('uid');
    role = prefs.getString('role');
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}