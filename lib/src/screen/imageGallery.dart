import 'dart:convert';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'homeNotLogin.dart';

class ImageGallery extends StatefulWidget {
  @override
  _ImageGalleryState createState() => _ImageGalleryState();
}

class _ImageGalleryState extends State<ImageGallery> with AutomaticKeepAliveClientMixin {
  @override
  bool get wantKeepAlive => true;
  String uid,color;
  Color colorThemeApp;
  bool loadUserProfile = true;

  @override
  void initState() {
    loadProfile();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return DefaultTabController(
      length: 2,
      initialIndex: 0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Container(
            child: TabBar(
              indicatorColor: colorThemeApp,
              labelColor: colorThemeApp,
              unselectedLabelColor: Colors.black,
              tabs: [
                Tab(text: 'รูปสาธารณะ'),
                Tab(text: 'รูปส่วนตัว')
              ],
            ),
          ),
          Expanded(
            child: TabBarView(
              children: [
                SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: StreamBuilder(
                      stream: Firestore.instance.collection('shareImage').where('status',isEqualTo: 'public').snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data.documents.length > 0 ? ListView.separated(
                            separatorBuilder: (context, index) => Divider(color: Colors.grey),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context, i) {
                              var data = snapshot.data.documents[i];
                              return Card(
                                child: Column(
                                  children: [
                                    Image.memory(
                                      base64Decode(data['image'])
                                    ),
                                    Row(
                                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                                      children: [
                                        IconButton(
                                          icon: Icon(Icons.file_download),
                                          color: colorThemeApp,
                                          onPressed: () async {
                                            await ImageGallerySaver.saveImage(
                                              Uint8List.fromList(base64Decode(data['image'])),
                                              quality: 80,
                                              name: DateTime.now().millisecondsSinceEpoch.toString()
                                            );
                                          }
                                        ),
                                        IconButton(
                                          icon: data['vertify'] == 'canReport' ? Icon(Icons.error_rounded) : data['vertify'] == 'review' ? Icon(Icons.preview_outlined ): Icon(Icons.check_circle),
                                          color: colorThemeApp,
                                          onPressed: data['vertify'] == 'canReport' ? () async {
                                            await Firestore.instance.collection('shareImage').document(data.documentID).updateData({
                                              'vertify' : 'review'
                                            });
                                          } : (){}
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              );
                            }
                          ) : Center(child: Text('ไม่พบข้อมูล'));
                        }
                        return Center(child: CircularProgressIndicator());
                      },
                    ),
                  ),
                ),
                SingleChildScrollView(
                  child: Padding(
                    padding: EdgeInsets.all(15),
                    child: uid == null ? Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        SizedBox(height: MediaQuery.of(context).size.height * 0.1),
                        Image.asset('assets/images/notLogin.png'),
                        Container(
                          child: RaisedButton(
                            color: colorThemeApp,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(20)
                            ),
                            onPressed: () => Navigator.push(context, MaterialPageRoute(builder: (context) => HomeNotLogin())),
                            child: Text(
                              'เข้าสู่ระบบ',
                              style: TextStyle(
                                color: color == 'purple' || color == 'red' || color == 'pink' ? Colors.white : Colors.black,
                              ),
                            ),
                          ),
                        )
                      ],
                    ) : StreamBuilder(
                      stream: Firestore.instance.collection('shareImage').where('uploader',isEqualTo: uid).where('status',isEqualTo: 'private').snapshots(),
                      builder: (context, snapshot) {
                        if (snapshot.hasData) {
                          return snapshot.data.documents.length > 0 ? ListView.separated(
                            separatorBuilder: (context, index) => Divider(color: Colors.grey),
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: snapshot.data.documents.length,
                            itemBuilder: (context, i) {
                              var data = snapshot.data.documents[i];
                              return Card(
                                child: Column(
                                  children: [
                                    Image.memory(
                                      base64Decode(data['image'])
                                    ),
                                    IconButton(
                                      icon: Icon(Icons.file_download),
                                      color: colorThemeApp,
                                      onPressed: () async {
                                        await ImageGallerySaver.saveImage(
                                          Uint8List.fromList(base64Decode(data['image'])),
                                          quality: 80,
                                          name: DateTime.now().millisecondsSinceEpoch.toString()
                                        );
                                      }
                                    )
                                  ],
                                ),
                              );
                            }
                          ) : Center(child: Text('ไม่พบข้อมูล'));
                        }
                        return Center(child: CircularProgressIndicator());
                      },
                    ),
                  ),
                ),
              ],
            ),
          )
        ],
      )
    );
  }

    loadProfile() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    uid = prefs.getString('uid');
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadUserProfile = false;
    });
  }
}