import 'package:captionary/src/model/facebookSignIn.dart';
import 'package:captionary/src/model/googleSignIn.dart';
import 'package:captionary/src/model/lineSignIn.dart';

class Auth {
  Future signInWithGoogle() async {
    return await Google().signInWithGoogle();
  }

  Future signOutGoogle() async {
    Google().signOutGoogle();
  }

  Future signInWithFacebook() async {
    return await Facebook().signInWithFacebook();
  }

  Future signOutFacebook() async {
    Facebook().signOutFacebook();
  }

  Future signInWithLine() async {
    return await LineSignIN().signInWithLine();
  }

  Future signOutLine() async {
    LineSignIN().signOutLine();
  }
}