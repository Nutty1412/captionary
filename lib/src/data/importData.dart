import 'package:captionary/src/data/data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ImportData extends StatefulWidget {
  ImportData({Key key}) : super(key: key);

  @override
  _ImportDataState createState() => _ImportDataState();
}

class _ImportDataState extends State<ImportData> {
  @override
  Widget build(BuildContext context) {
    var dataImport = Data().data;
    return Scaffold(
      body: Center(
        child: RaisedButton(
          child: Text(dataImport.length.toString()),
          onPressed: () async {
            for (int j=0 ; j < dataImport.length ; j++) {
              await Firestore.instance.collection("captions").document().setData({
                "message": dataImport[j]['message'],
                "category": dataImport[j]['category'],
                "credit": dataImport[j]['credit'],
                "postBy": "Admin",
                "status": "canReport",
                "like": [],
                "comment": [],
                "countShare" : 0,
                "dateCreate" : DateTime.now().microsecondsSinceEpoch,
                "lastUpdate": DateTime.now().microsecondsSinceEpoch
              });
            }
          }
        ),
      ),
    );
  }
}