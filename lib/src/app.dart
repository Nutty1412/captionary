import 'package:captionary/src/screen/userPage.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class App extends StatefulWidget {
  App({
    this.defaultRoute = 0
  });

  final defaultRoute;
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  String theme,color;
  bool loadTheme = false;
  int defaultRoute;
  Color colorThemeApp;

  @override
  void initState() {
    defaultRoute = this.widget.defaultRoute;
    super.initState();
    loadThemeApp();
  }
  @override
  Widget build(BuildContext context) {
    return loadTheme ? MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Captionary',
      theme: ThemeData(
        brightness: theme == 'light' ? Brightness.light : Brightness.dark,
        scaffoldBackgroundColor: theme == 'light' ? Colors.white : null,
        fontFamily: 'Mali',
        primaryColor: colorThemeApp,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: UserPage(defaultRoute: defaultRoute,color: colorThemeApp)
    ) : MaterialApp(
      home: Scaffold(
        body: Center(),
      ),
    );
  }

  loadThemeApp() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if (prefs.getString('theme') == null) {
      prefs.setString('theme', 'light');
      theme = prefs.getString('theme');
    } else {
      theme = prefs.getString('theme');
    }
    if (prefs.getString('color') == null) {
      prefs.setString('color', 'pink');
      color = prefs.getString('color');
    } else {
      color = prefs.getString('color');
    }
    if (color == 'pink') {
      colorThemeApp = Color(0xFFfd3a69);
    } else if (color == 'purple') {
      colorThemeApp = Color(0xFF8F00F2);
    } else if (color == 'blue') {
      colorThemeApp = Color(0xFF00CFFB);
    } else if (color == 'green') {
      colorThemeApp = Color(0xFF5CFF00);
    } else if (color == 'yellow') {
      colorThemeApp = Color(0xFFFDFB00);
    } else if (color == 'orange') {
      colorThemeApp = Color(0xFFFDAE32);
    } else if (color == 'red') {
      colorThemeApp = Color(0xFFFF0C12);
    }
    setState(() {
      loadTheme = true;
    });
  }
}