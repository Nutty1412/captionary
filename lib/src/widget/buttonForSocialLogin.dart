import 'package:flutter/material.dart';

class ButtonForSocialLogin extends StatefulWidget {
  final icon,text,color,fontColor;

  ButtonForSocialLogin({
    @required this.icon,
    @required this.text,
    @required this.color,
    @required this.fontColor
  });

  @override
  _ButtonForSocialLoginState createState() => _ButtonForSocialLoginState();
}

class _ButtonForSocialLoginState extends State<ButtonForSocialLogin> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: width * 0.1),
      child: Container(
        decoration: BoxDecoration(
          color: this.widget.color,
          borderRadius: BorderRadius.circular(10)
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: width * 0.0125),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(left: 20,right: 5),
                child: Image.asset(
                  this.widget.icon,
                  height: 64,
                ),
              ),
              Text(
                this.widget.text,
                style: TextStyle(
                  color: this.widget.fontColor,
                  fontWeight: FontWeight.bold
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}